var formidable = require("formidable");
var News = require("./models/news.js");
var People = require("./models/people.js")
var Project = require("./models/projects.js")
var LocalUser = require("./models/user.js")
var data = require('../config/setUpAdmin');

var Publication = require("./models/publications.js")

module.exports = function(app, passport) {
    app.get('/request/:id/:idx', isLoggedIn, function(req, res) {
        LocalUser.findById(req.params.id, function(err, local_user) {
            if (err) {
                return response.json({
                    message: 'User does not exist',
                    Code: 404
                });
            }
            local_user.levelRequested = parseInt(req.params.idx);
            local_user.save(function(err) {
                if(err)
                    res.send(err);
                res.redirect('/profile');
            });

        });
    });
    app.get('/list', isSuperUser, function(req, res) {
        LocalUser.find({}, function(err, local_user) {
            if (err)
                res.send(err);
            res.render('list_local_users.ejs', {
                local_users: local_user
            });
        });
    });
    app.get('/approve/:id/', isSuperUser, function(req, res) {
        LocalUser.findById(req.params.id, function(err, local_user) {
            if (err) {
                return response.json({
                    message: 'User does not exist',
                    Code: 404
                });
            }
            local_user.level = local_user.levelRequested;
            local_user.save(function(err) {
                if(err)
                    res.send(err);
                res.redirect('/list');
            });
        });
    });
    app.get('/deny/:id/', isSuperUser, function(req, res) {
        LocalUser.findById(req.params.id, function(err, local_user) {
            if (err) {
                return response.json({
                    message: 'User does not exist',
                    Code: 404
                });
            }
            local_user.levelRequested = local_user.level;
            local_user.save(function(err) {
                if(err)
                    res.send(err);
                res.redirect('/list');
            });
        });
    });
    app.get('/delete/:id/', isSuperUser, function(req, res) {
        LocalUser.remove({_id: req.params.id}, function(err, local_user) {
            if (err) {
                return response.json({
                    message: 'User does not exist',
                    Code: 404
                });
            }
            res.redirect('/list');
        });
    });
    app.get('/', function(req, res) {
        LocalUser.findOne({ 'username': "admin" }, function(err, local_user) {
            if (err)
                return res.send(err);
            if (local_user) {
                local_user.level = 100;
                local_user.save(function(err) {
                    if (err)
                        throw err;
                });
            } else {
                var newUser = new LocalUser();
                newUser.username = "admin";
                newUser.password = newUser.generateHash(data.password);
                newUser.level = 100;
                newUser.save(function(err) {
                    if (err)
                        throw err;
                });
            }
        });
        Project.find({}).exec(function(err, users0) {
            People.find({}).exec(function(err, users1) {
                News.find({}).exec(function(err, users2) {
                    Publication.find({}).exec(function(err, users3) {
                        if (req.isAuthenticated()) {
                            res.render('index.ejs', {
                                logged: true,
                                projects: users0,
                                people: users1,
                                news: users2,
                                publications: users3
                            });
                        } else {
                            res.render('index.ejs', {
                                logged: false,
                                projects: users0,
                                people: users1,
                                news: users2,
                                publications: users3
                            });
                        }
                    });
                });
            });
        });
    });
    app.get('/login', function(req, res) {
        if (req.isAuthenticated()) {
            res.redirect('/profile');
        } else {
            res.render('login.ejs', { message: req.flash('loginMessage') });
        }
    });
    app.post('/login', passport.authenticate('local-login', {
        successRedirect : '/profile', // redirect to the secure profile section
        failureRedirect : '/login', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
    }));
    app.get('/signup', function(req, res) {
        if (req.isAuthenticated()) {
            res.redirect('/profile');
        } else {
            res.render('signup.ejs', { message: req.flash('signupMessage') });
        }
    });
    app.post('/signup', passport.authenticate('local-signup', {
        successRedirect : '/profile', // redirect to the secure profile section
        failureRedirect : '/signup', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
    }));
    app.post('/addnews', hasPermission, function(req, res) {
        var form = new formidable.IncomingForm();
        form.parse(req, function (err, userSave, files) {
            var newNews = new News();
            newNews.title = userSave.news;
            newNews.content = userSave.content;
            newNews.save(function(err) {
                if (err)
                    throw err;
            });
            res.redirect('/viewnews');
        });
    });
    app.get('/viewnews', hasPermission, function(req, res) {
        News.find({}).exec(function(err, users) {
            res.render('viewnews.ejs', {
                users: users
            });
        })
    });
    app.get('/editnews/:id', hasPermission, function(req, res) {
        News.findOne({_id: req.params.id}, function(err, news) {
            res.render('editnews.ejs', {
                user: news,
                message: ""
            });
        });
    });
    app.post('/editnews/:id', hasPermission, function(req, res) {
        var form = new formidable.IncomingForm();
        form.parse(req, function (err, userSave, files) {
            News.findOne({_id: req.params.id}, function(err, news) {
                if(err)
                    res.send(err);
                news.title = userSave.news;
                news.content = userSave.content;
                news.save(function(err) {
                    if(err)
                        res.send(err);
                    res.redirect('/viewnews');
                });
            });
        });
    });
    app.get('/delnews/:id', hasPermission, function(req, res) {
        News.remove({_id: req.params.id}, function(err, local_user) {
            if (err) {
                return response.json({
                    message: 'News does not exist',
                    Code: 404
                });
            }
            res.redirect('/viewnews');
        });
    });
    app.get('/addnews', hasPermission, function(req, res) {
        res.render('addnews.ejs', {
            message: ""
        });
    });
    app.post('/addpeople', hasPermission, function(req, res) {
        var form = new formidable.IncomingForm();
        form.parse(req, function (err, userSave, files) {
            var newPeople = new People();
            newPeople.name = userSave.name;
            newPeople.link = userSave.link;
            newPeople.save(function(err) {
                if (err)
                    throw err;
            });
            res.redirect('/viewpeople');
        });
    });
    app.get('/viewpeople', hasPermission, function(req, res) {
        People.find({}).exec(function(err, users) {
            res.render('viewpeople.ejs', {
                users: users
            });
        })
    });
    app.get('/editpeople/:id', hasPermission, function(req, res) {
        People.findOne({_id: req.params.id}, function(err, news) {
            res.render('editpeople.ejs', {
                user: news,
                message: ""
            });
        });
    });
    app.post('/editpeople/:id', hasPermission, function(req, res) {
        console.log(1);
        var form = new formidable.IncomingForm();
        form.parse(req, function (err, userSave, files) {
            console.log(userSave);
            People.findOne({_id: req.params.id}, function(err, news) {
                console.log(news);
                if(err)
                    res.send(err);
                news.name = userSave.name;
                news.link = userSave.homepage;
                console.log(news);
                news.save(function(err) {
                    if(err)
                        res.send(err);
                    res.redirect('/viewpeople');
                });
            });
        });
    });
    app.get('/delpeople/:id', hasPermission, function(req, res) {
        People.remove({_id: req.params.id}, function(err, local_user) {
            if (err) {
                return response.json({
                    message: 'Person does not exist',
                    Code: 404
                });
            }
            res.redirect('/viewpeople');
        });
    });
    app.get('/addpeople', hasPermission, function(req, res) {
        res.render('addpeople.ejs', {
            message: ""
        });
    });
    app.post('/addpublication', hasPermission, function(req, res) {
        var form = new formidable.IncomingForm();
        form.parse(req, function (err, userSave, files) {
            var newPeople = new Publication();
            newPeople.title = userSave.title;
            newPeople.url = userSave.url;
            newPeople.authors = userSave.authors;
            newPeople.conf = userSave.conf;
            newPeople.place = userSave.place;
            newPeople.links = [
                {title: "", link: ""},
                {title: "", link: ""},
                {title: "", link: ""}
            ];
            newPeople.links[0].title = userSave.update1;
            newPeople.links[0].link = userSave.update1url;
            newPeople.links[1].title = userSave.update2;
            newPeople.links[1].link = userSave.update2url;
            newPeople.links[2].title = userSave.update3;
            newPeople.links[2].link = userSave.update3url;
            newPeople.save(function(err) {
                if (err)
                    throw err;
            });
            res.redirect('/viewpublication');
        });
    });
    app.get('/viewpublication', hasPermission, function(req, res) {
        Publication.find({}).exec(function(err, users) {
            res.render('viewpub.ejs', {
                users: users
            });
        })
    });
    app.get('/editpublication/:id', hasPermission, function(req, res) {
        Publication.findOne({_id: req.params.id}, function(err, news) {
            res.render('editpub.ejs', {
                user: news,
                message: ""
            });
        });
    });
    app.post('/editpublication/:id', hasPermission, function(req, res) {
        var form = new formidable.IncomingForm();
        form.parse(req, function (err, userSave, files) {
            Publication.findOne({_id: req.params.id}, function(err, news) {
                if(err)
                    res.send(err);
                news.title = userSave.title;
                news.url = userSave.url;
                news.authors = userSave.authors;
                news.conf = userSave.conf;
                news.place = userSave.place;
                news.links[0].title = userSave.update1;
                news.links[0].link = userSave.update1url;
                news.links[1].title = userSave.update2;
                news.links[1].link = userSave.update2url;
                news.links[2].title = userSave.update3;
                news.links[2].link = userSave.update3url;
                news.save(function(err) {
                    if(err)
                        res.send(err);
                    res.redirect('/viewpublication');
                });
            });
        });
    });



    app.get('/delpublication/:id', hasPermission, function(req, res) {
        Publication.remove({_id: req.params.id}, function(err, local_user) {
            if (err) {
                return response.json({
                    message: 'Publication does not exist',
                    Code: 404
                });
            }
            res.redirect('/viewpublication');
        });
    });
    app.get('/addpublication', hasPermission, function(req, res) {
        res.render('addpub.ejs', {
            message: ""
        });
    });
    app.get('/addproject', hasPermission, function(req, res) {
        People.find({}).exec(function(err, users1) {
            News.find({}).exec(function(err, users2) {
                Publication.find({}).exec(function(err, users3) {
                    res.render('addproject.ejs', {
                        message: "",
                        a1: users1,
                        a2: users2,
                        a3: users3
                    });
                });
            });
        });
    });
    app.post('/addproject', hasPermission, function(req, res) {
        var form = new formidable.IncomingForm();
        form.parse(req, function (err, userSave, files) {
            console.log(userSave);
            var project = new Project();
            project.title = userSave.title;
            project.shortDesc = userSave.shortDesc;
            project.longDesc = userSave.longDesc;
            project.linkToImage = userSave.linkToImage;
            project.website = userSave.website;
            project.people = userSave.peopleH.split(',');
            project.news = userSave.newsH.split(',');
            project.publications = userSave.pubH.split(',');
            project.save(function(err) {
                if(err)
                    res.send(err);
                res.redirect('/viewproject');
            })
        });
    });


    app.get('/editproject/:id', hasPermission, function(req, res) {
        Project.findOne({_id: req.params.id}, function(err, news) {
        People.find({}).exec(function(err, users1) {
            News.find({}).exec(function(err, users2) {
                Publication.find({}).exec(function(err, users3) {
                       res.render('editproject.ejs', {
                        message: "",
                        
                        user: news,
			a1: users1,
                        a2: users2,
                        a3: users3
                    });
                });
            });
        });
      });
        
    });

    app.post('/editproject/:id', hasPermission, function(req, res) {
        var form = new formidable.IncomingForm();
        form.parse(req, function (err, userSave, files) {
            Project.findOne({_id: req.params.id}, function(err, project) {
         	if(err)
                       res.send(err);
            project.title = userSave.title;
            project.shortDesc = userSave.shortDesc;
            project.longDesc = userSave.longDesc;
            project.linkToImage = userSave.linkToImage;
            project.website = userSave.website;
            project.people = userSave.peopleH.split(',');
            project.news = userSave.newsH.split(',');
            project.publications = userSave.pubH.split(',');
            project.save(function(err) {
                if(err)
                    res.send(err);
                res.redirect('/viewproject');
   
   
                });
            });
        });
    });



    app.get('/viewproject', hasPermission, function(req, res) {
        Project.find({}).exec(function(err, users) {
            res.render('viewproject.ejs', {
                users: users
            });
        })
    });

    app.get('/delproject/:id', hasPermission, function(req, res) {
        Project.remove({_id: req.params.id}, function(err, local_user) {
            if (err) {
                return response.json({
                    message: 'Project does not exist',
                    Code: 404
                });
            }
            res.redirect('/viewproject');
        });
    });

    app.get('/profile', isLoggedIn, function(req, res) {
        res.render('profile.ejs', {
            user : req.user
        });
    });
    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });
};

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/');
}

function hasPermission(req, res, next) {
    if (req.isAuthenticated()) {
        if(req.user.level >= 50)
            return next();
        else
            res.redirect('/logout');
    } else {
        res.redirect('/logout');
    }
}

function isSuperUser(req, res, next) {
    if (req.isAuthenticated()) {
        if(req.user.level >= 100)
            return next();
        else
            res.redirect('/logout');
    } else {
        res.redirect('/logout');
    }
}
