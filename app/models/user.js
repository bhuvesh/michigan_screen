var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

var userSchema = mongoose.Schema({
        username: String,
        password: String,
    level: {
        type: Number,
        default: 0 //no access
    },
    levelRequested: {
        type: Number,
        default: 0
    }
});

userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

userSchema.pre('save', function(next) {
    var local_user = this;
    if(local_user.levelRequested < local_user.level) {
        local_user.levelRequested = local_user.level;
    }
    next();
})

module.exports = mongoose.model('User', userSchema);
