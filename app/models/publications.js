var mongoose = require('mongoose');

var publicationSchema = mongoose.Schema({
    title: String,
    url: String,
    conf: String,
    place: String,
    authors: String,
    links: [{
        title: String,
        link: String
    }]
});

module.exports = mongoose.model('Publication', publicationSchema);
