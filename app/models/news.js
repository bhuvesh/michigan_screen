var mongoose = require('mongoose');

var newsSchema = mongoose.Schema({
    title: String,
    content: String,
});

module.exports = mongoose.model('News', newsSchema);
