var mongoose = require('mongoose');

var peopleSchema = mongoose.Schema({
    name: String,
    link: String,
});

module.exports = mongoose.model('People', peopleSchema);
