var mongoose = require('mongoose');

var projectSchema = mongoose.Schema({
    title: String,
    shortDesc: String,
    longDesc: String,
    website: String,
    linkToImage: String,
    people: [],
    news: [],
    publications: []
});

module.exports = mongoose.model('Project', projectSchema);
